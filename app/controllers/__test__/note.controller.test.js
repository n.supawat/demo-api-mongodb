jest.unmock('../note.controller')
const mockSave = jest.fn()
const mockNoteModel = jest.fn().mockName('NoteModel')


//Create Mock Constant for Model
jest.mock('../../models/note.model', () => {
    return jest.fn().mockImplementation(() => {
        return { Note: mockNoteModel, save: mockSave }
    })
})

// const Note = require('../../models/note.model')
const noteController = require('../note.controller')

//Create Mock response
const res = {
    status: jest.fn(),
    send: jest.fn()
}
res.status.mockImplementation(() => res)

describe('Note Controller', () => {
    describe('Create Note', () => {

        beforeEach(() => {
            jest.clearAllMocks()
        });

        it('should return res error when req empty', async () => {
            //arrange
            const req = { body: { content: "" } }

            //act
            await noteController.create(req, res)//.then(console.log(res)).catch(console.log(res))
                .then(() => {
                    //assert
                    expect(res.status).toHaveBeenCalled()
                    expect(res.status).toHaveBeenCalledWith(400)
                    expect(res.send).toHaveBeenCalledWith({
                        message: "Note content can not be empty"
                    })
                })
        })

        it('should be save', async () => {
            //arrange
            const req = { body: { title: "hello", content: "test" } }
            mockSave.mockImplementation(() => Promise.resolve("test"))

            //act
            await noteController.create(req, res)

            //assert
            expect(mockSave).toHaveBeenCalled()
            // console.log(mockSave.mock.instances[0])
            expect(res.send).toHaveBeenCalled()

        })

        it('should be edit empty title with Untitled Note', async () => {
            //arrange
            const req = { body: { title: "", content: "test" } }
            mockSave.mockImplementation(() => Promise.resolve("test"))

            //act
            await noteController.create(req, res)

            //assert
            expect(mockSave).toHaveBeenCalled()
            expect(mockSave.mock.instances[0].title).toBe("Untitled Note")
            expect(res.send).toHaveBeenCalled()

        })
        it('should Handle error with out message', async () => {
            //arrange
            const req = { body: { title: "hello", content: "test" } }
            mockSave.mockImplementation(() => Promise.reject("error"))

            //act
            await noteController.create(req, res)

            expect(mockSave).toHaveBeenCalled()
            expect(res.status).toHaveBeenCalledWith(500)
            expect(res.send).toHaveBeenCalledWith({
                message : "Some error occurred while creating the Note."
            })

        })

        it('should Handle error with out error.message', async () => {
            //arrange
            const req = { body: { title: "hello", content: "test" } }
            mockSave.mockImplementation(() => Promise.reject({message:"error"}))

            //act
            await noteController.create(req, res)

            expect(mockSave).toHaveBeenCalled()
            expect(res.status).toHaveBeenCalledWith(500)
            expect(res.send).toHaveBeenCalledWith({
                message : "error"
            })

        })

    })
})