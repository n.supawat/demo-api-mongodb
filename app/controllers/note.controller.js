const Note = require('../models/note.model')

//Create and Save Model
exports.create = async (req, res) => {
    // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }
    // Create a Note
    // const note = new Note({
    //     title: req.body.title || "Untitled Note", 
    //     content: req.body.content
    // });

    const note = new Note()
    note.title = req.body.title || "Untitled Note"
    note.content = req.body.content
    // Save Note in the database
    await note.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });

};


// 