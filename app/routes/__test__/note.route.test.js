jest.unmock('../note.routes')
jest.unmock('supertest')
jest.mock('express',() => {
    return require('jest-express')
})

const mockSave = jest.fn()
const mockNoteModel = jest.fn().mockName('NoteModel')
//Create Mock Constant for Model
jest.mock('../../models/note.model', () => {
    return jest.fn().mockImplementation(() => {
        return { Note: mockNoteModel, save: mockSave }
    })
})

const request = require('supertest')
const notes = require('../../controllers/note.controller')



describe('test note routes', () => {
    
    it('should represent post method /notes', async () => {
        //arrange
        const app = {post:jest.fn()}
        //act
        require('../note.routes')(app)
        //assert
        expect(app.post).toHaveBeenCalled()
        expect(app.post).toHaveBeenCalledWith("/notes",notes.create)
    })
})