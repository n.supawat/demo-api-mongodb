
const express = require('express');
const bodyParser = require('body-parser');
// Configuring the database
const dbConfig = require('./config/database.config')
const mongoose = require('mongoose')



mongoose.Promise = global.Promise

//Connect To DB
mongoose.connect(dbConfig.url)
.then(() => {
    console.log("connect db success")
}).catch(err => {
    console.log("cannot connect to db server", err)
    process.exit()
})

// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

require('./app/routes/note.routes')(app)
// listen for requests
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
})